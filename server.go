package main

import (
	"fmt"

	"gitlab.com/ishangoyal/goapi/routers"
	// jwtAuth "gitlab.com/ishangoyal/goapi/services"
)

func main() {
	router := routers.RegisterRoutes()
	// fmt.Printf(jwtAuth.Auth())

	fmt.Printf("\nSuccessfully connected to database :)\n\n")

	router.Run("localhost:8080")
}
